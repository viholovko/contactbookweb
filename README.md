React Project: Contact Book WEB (React + Redux)   
===========================================
  The main idea of this project to show knowledge and experience to React. All users have the possibility to create and 
manage personal protected contact, in this project.

## Node/NPM Versions

I'm running node v8.10.0 and npm v5.3.0 as I tinker, there's no plan to
support older versions at the moment.

**Running Locally**
  * Install NodeJS and NPM from https://nodejs.org/en/download/.
  * Install all required npm packages by running npm install from the command line in the project root folder (where the package.json is located). (npm install) 
  * Start the application by running npm start from the command line in the project root folder, this will launch a browser displaying the application. (npm start)
  
## Getting Started
  
````
cd project_folder
npm install
npm start
````
  
Now open [http://localhost:8080](http://localhost:8080).