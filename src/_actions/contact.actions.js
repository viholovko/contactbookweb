import {contactConstants} from '../_constants';
import {contactService} from '../_services';
import {history} from "../_helpers";
import {alertActions} from "./alert.actions";

export const contactActions = {
    getAll,
    create,
    update,
    delete: _delete
};

function getAll(params) {
    return dispatch => {
        dispatch(request());

        contactService.getAll(params)
            .then(
                contacts => dispatch(success(contacts)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: contactConstants.GETALL_REQUEST } }
    function success(contacts) { return { type: contactConstants.GETALL_SUCCESS, contacts } }
    function failure(error) { return { type: contactConstants.GETALL_FAILURE, error } }
}

function create(contact) {
    return dispatch => {
        dispatch(request());

        contactService.create(contact)
          .then(
            contact => {
                dispatch(success(contact));
                dispatch(alertActions.success('Contact created successfully.'));
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
          );
    };

    function request() { return { type: contactConstants.CREATE_REQUEST } }
    function success(contact) { return { type: contactConstants.CREATE_SUCCESS, contact } }
    function failure(error) { return { type: contactConstants.CREATE_FAILURE, error } }
}

function update(contact) {
    return dispatch => {
        dispatch(request());

        contactService.update(contact)
          .then(
            contact => {
                dispatch(success(contact));
                dispatch(alertActions.success('Contact updated successfully.'));
            },
            error => {
                dispatch(failure(error.toString()));
                dispatch(alertActions.error(error.toString()));
            }
          );
    };

    function request() { return { type: contactConstants.UPDATE_REQUEST } }
    function success(contact) { return { type: contactConstants.UPDATE_SUCCESS, contact } }
    function failure(error) { return { type: contactConstants.UPDATE_FAILURE, error } }
}

function _delete(id) {
    return dispatch => {
        dispatch(request(id));

        contactService.delete(id)
          .then(
            contact => {
                dispatch(success(id))
                dispatch(alertActions.success('Contact deleted successfully.'));
            },
            error => dispatch(failure(id, error.toString()))
          );
    };

    function request(id) { return { type: contactConstants.DELETE_REQUEST, id } }
    function success(id) { return { type: contactConstants.DELETE_SUCCESS, id } }
    function failure(id, error) { return { type: contactConstants.DELETE_FAILURE, id, error } }
}