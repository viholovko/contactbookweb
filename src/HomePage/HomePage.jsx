import React from 'react';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';

import {contactActions} from '../_actions';

class HomePage extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      contact: {
        firstName: '',
        lastName: '',
        email: '',
        phone: '',
        isFavourite: false
      },
      editContact: false,
      page: 1,
      per_page: 10
    };

    this.handleEditContact = this.handleEditContact.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangePreviousPage = this.handleChangePreviousPage.bind(this);
    this.handleChangeNextPage = this.handleChangeNextPage.bind(this);
  }

  handleChange(event) {
    const {name, value} = event.target;
    const {contact} = this.state;
    this.setState({
      contact: {
        ...contact,
        [name]: value
      }
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const {contact} = this.state;
    if (contact.firstName && contact.lastName && contact.email && contact.phone) {
      if (contact && contact.id) {
        this.handleEditContact;
        this.props.updating(contact);
      } else {
        this.handleEditContact;
        this.props.creating(contact);
      }
    }
  }

  componentDidMount() {
    this.props.getContacts({page: 1, per_page: 10});
  }

  handleChangePreviousPage(event) {
    event.preventDefault();
    this.setState({page: this.state.page-1});
    this.props.getContacts({page: this.state.page-1, per_page: 10});
  }

  handleChangeNextPage(event) {
    event.preventDefault();
    this.setState({page: this.state.page+1});
    this.props.getContacts({page: this.state.page+1, per_page:  10});
  }


  handleDeleteContact(id) {
    return (e) => this.props.deleteContact(id);
  }

  handleSelectContact(contact) {
    this.setState({contact: contact, editContact: true})
  }

  handleEditContact() {
    this.setState({editContact: !this.state.editContact, contact: {}})
  }

  render() {
    const {user, contacts} = this.props;
    const {editContact, contact, page, per_page} = this.state;
    return (
      <div className="col-md-6 col-md-offset-3">
        <h1>Hi {user.firstName}!</h1>
        <p>
          <Link to="/login">Logout</Link>
        </p>
        {contacts.loading && <em>Loading contacts...</em>}
        {contacts.error && <span className="text-danger">ERROR: {contacts.error}</span>}

        <div className="form-group">
          <button className="btn btn-primary" onClick={this.handleEditContact} disabled={editContact}>Add New Contact
          </button>
        </div>

        {editContact && !contacts.created &&
        <div className="col-md-12 col-md-offset-3">
          <h2>{contact && contact.id ? 'Edit Contact' : 'Contact'}</h2>
          <form name="form" onSubmit={this.handleSubmit}>
            <div className={'form-group' + (contacts.creating && !contact.firstName ? ' has-error' : '')}>
              <label htmlFor="firstName">First Name</label>
              <input type="text" className="form-control" name="firstName" value={contact.firstName}
                     onChange={this.handleChange}/>
              {contacts.creating && !contact.firstName &&
              <div className="help-block">First Name is required</div>
              }
            </div>
            <div className={'form-group' + (contacts.creating && !contact.lastName ? ' has-error' : '')}>
              <label htmlFor="lastName">Last Name</label>
              <input type="text" className="form-control" name="lastName" value={contact.lastName}
                     onChange={this.handleChange}/>
              {contacts.creating && !contact.lastName &&
              <div className="help-block">Last Name is required</div>
              }
            </div>
            <div className={'form-group' + (contacts.creating && !contact.email ? ' has-error' : '')}>
              <label htmlFor="email">Email</label>
              <input type="email" className="form-control" name="email" value={contact.email}
                     onChange={this.handleChange}/>
              {contacts.creating && !contact.email &&
              <div className="help-block">Email is required</div>
              }
            </div>
            <div className={'form-group' + (contacts.creating && !contact.phone ? ' has-error' : '')}>
              <label htmlFor="phone">Phone</label>
              <input type="tel" className="form-control" name="phone" value={contact.phone}
                     onChange={this.handleChange}/>
            </div>
            <div className='form-group'>
              <label htmlFor="isFavourite">Favourite
              <input type="checkbox" className="form-control" name="isFavourite" value={contact.isFavourite}
                     defaultChecked={contact.isFavourite}
                     onChange={this.handleChange}/>
              </label>
            </div>
            <div className="form-group">
              <button className="btn btn-primary" disabled={contacts.creating}>
                {contact && contact.id ? 'Update Contact' : 'Add Contact'}
              </button>
              {contacts.creating &&
              <img
                src="data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA=="/>
              }
              <Link to="/" className="btn btn-link" disabled={contacts.creating}
                    onClick={this.handleEditContact}>Cancel</Link>
            </div>
          </form>
        </div>
        }

        <table className="table">
          <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
            <th scope="col">Email</th>
            <th scope="col">Phone</th>
            <th scope="col">Is Favourite</th>
            <th scope="col">Action</th>
          </tr>
          </thead>
          <tbody>
          {contacts.items && contacts.items.map((contact, index) =>
            <tr key={contact.id}>
              <th scope="row">{(page-1)*per_page+(index + 1)}</th>
              <td>{contact.firstName}</td>
              <td>{contact.lastName}</td>
              <td>{contact.email}</td>
              <td>{contact.phone}</td>
              <td>{contact.isFavourite ?'Favourite':''}</td>
              <td>
                <span> <a onClick={() => this.handleSelectContact(contact)}>Edit</a></span>
                {
                  contact.deleting ? <em> Deleting...</em>
                    : contact.deleteError ? <span className="text-danger"> - ERROR: {contact.deleteError}</span>
                    : <span> <a onClick={this.handleDeleteContact(contact.id)}>Delete</a></span>
                }
              </td>
            </tr>
          )}
          </tbody>
        </table>
        <div className="form-group">
          <span> <button className="btn btn-primary" onClick={this.handleChangePreviousPage}
                         disabled={page === 1}>Previous</button></span>
          <span>  Page {page}</span>
          <span> <button className="btn btn-primary" onClick={this.handleChangeNextPage}
                         disabled={contacts.items && (page*per_page)>contacts.items.length }>Next</button></span>
          {contacts.count &&
            <span>  In your list {contacts.count} contact</span>
          }
        </div>
      </div>
    );
  }
}

function mapState(state) {
  const {contacts, authentication, contact} = state;
  const {user} = authentication;
  return {user, contacts};
}

const actionCreators = {
  getContacts: contactActions.getAll,
  deleteContact: contactActions.delete,
  creating: contactActions.create,
  updating: contactActions.update
}

const connectedHomePage = connect(mapState, actionCreators)(HomePage);
export {connectedHomePage as HomePage};