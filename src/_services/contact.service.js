import config from 'config';
import { authHeader } from '../_helpers';

export const contactService = {
    getAll,
    create,
    update,
    delete: _delete
};

function getAll({page,per_page}) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/api/v1/contacts?page=${page}&per_page=${per_page}`, requestOptions).then(handleResponse);
}

function create(contact) {
    const requestOptions = {
        method: 'POST',
        headers: authHeader(),
        body: JSON.stringify(contact)
    };

    return fetch(`${config.apiUrl}/api/v1/contacts`, requestOptions).then(handleResponse);
}

function update(contact) {
    const requestOptions = {
        method: 'PUT',
        headers: authHeader(),
        body: JSON.stringify(contact)
    };

    return fetch(`${config.apiUrl}/api/v1/contacts/${contact.id}`, requestOptions).then(handleResponse);
}

function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/api/v1/contacts/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}