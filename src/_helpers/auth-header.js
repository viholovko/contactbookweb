export function authHeader() {
    // return authorization header with jwt token
    let user = JSON.parse(localStorage.getItem('user'));
    let sessionToken = localStorage.getItem('session_token');

    if (sessionToken) {
        return { 'Content-Type': 'application/json', 'Session-Token': sessionToken };
    } else {
        return {};
    }

    if (user && user.token) {
        return { 'Authorization': 'Bearer ' + user.token };
    } else {
        return {};
    }
}