import { contactConstants } from '../_constants';
const initialState  = {
  items:[],
  count: null,
  creating:false,
  created: false
}
export function contacts(state = initialState, action) {

  switch (action.type) {
    case contactConstants.CREATE_REQUEST:
      return {
        ...state,
        creating: true
      };
    case contactConstants.CREATE_SUCCESS:
      return {
        ...state,
        created: true,
        items: [...state.items,action.contact.contact],
        count: state.count+1
      };
    case contactConstants.CREATE_FAILURE:
      return {
        ...state,
        creating: false,
      };
    case contactConstants.UPDATE_REQUEST:
      return {
        ...state,
        creating: true
      };
    case contactConstants.UPDATE_SUCCESS:
      const index = state.items.findIndex(emp => emp.id === action.contact.contact.id),
      items = [...state.items]
      items[index] = action.contact.contact;

      return {
        ...state,
        created: true,
        items: items,
        count: state.count
      };
    case contactConstants.UPDATE_FAILURE:
      return {
        ...state,
        creating: false,
      };
    case contactConstants.GETALL_REQUEST:
      return {
        loading: true
      };
    case contactConstants.GETALL_SUCCESS:
      return {
        items: action.contacts.contacts,
        count: action.contacts.count,
      };
    case contactConstants.GETALL_FAILURE:
      return {
        error: action.error
      };
    case contactConstants.DELETE_REQUEST:
      return {
        ...state,
        items: state.items.map(contact =>
          contact.id === action.id
            ? { ...contact, deleting: true }
            : contact
        )
      };
    case contactConstants.DELETE_SUCCESS:
      return {
        items: state.items.filter(contact => contact.id !== action.id),
        count: state.count-1
      };
    case contactConstants.DELETE_FAILURE:
      return {
        ...state,
        items: state.items.map(contact => {
          if (contact.id === action.id) {
            const { deleting, ...contactCopy } = contact;
            // return copy of user with 'deleteError:[error]' property
            return { ...contactCopy, deleteError: action.error };
          }

          return contact;
        })
      };
    default:
      return state
  }
}